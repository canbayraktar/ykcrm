DROP SCHEMA IF EXISTS yapikredi CASCADE;

CREATE SCHEMA yapikredi;

DROP TYPE IF EXISTS "yapikredi".approval_status;
CREATE TYPE "yapikredi".approval_status AS ENUM ('PENDING','APPROVED', 'REJECTED');

DROP TABLE IF EXISTS yapikredi.employers CASCADE;

CREATE TABLE yapikredi.employers
(
    id     uuid                                              NOT NULL,
    name   character varying COLLATE pg_catalog."default"    NOT NULL,
    surname   character varying COLLATE pg_catalog."default" NOT NULL,
    started_at      TIMESTAMP WITH TIME ZONE                 NOT NULL,
    CONSTRAINT employers_pkey PRIMARY KEY (id)
);


DROP TABLE IF EXISTS yapikredi.annual_leaves CASCADE;

CREATE TABLE yapikredi.annual_leaves
(
    id              uuid                                           NOT NULL,
    employer_id     uuid                                           NOT NULL,
    start_annual_date     TIMESTAMP WITH TIME ZONE                 NOT NULL,
    end_annual_date     TIMESTAMP WITH TIME ZONE                   NOT NULL,
    annualDays          integer,
    status              "yapikredi".approval_status                NOT NULL,
    CONSTRAINT annual_leaves_pkey PRIMARY KEY (id)
);