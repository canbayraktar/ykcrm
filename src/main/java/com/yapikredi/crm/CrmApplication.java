package com.yapikredi.crm;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

import java.time.*;

@EnableJpaRepositories(basePackages = { "com.yapikredi.crm.repository"})
@EntityScan(basePackages = { "com.yapikredi.crm.entity"})
@SpringBootApplication(scanBasePackages = "com.yapikredi.crm")
public class CrmApplication {
	public static void main(String[] args) {
		SpringApplication.run(CrmApplication.class, args);
	}
}