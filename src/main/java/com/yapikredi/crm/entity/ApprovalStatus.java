package com.yapikredi.crm.entity;

public enum ApprovalStatus {
    PENDING, CANCELLED, APPROVED
}