package com.yapikredi.crm.entity;

import lombok.*;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.time.Instant;
import java.util.Objects;
import java.util.UUID;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "employers")
@Entity
public class EmployerEntity {
    @Id
    private UUID id;
    private String name;
    private String surname;
    private Instant started_at;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        EmployerEntity employerEntity = (EmployerEntity) o;
        return Objects.equals(id, employerEntity.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}