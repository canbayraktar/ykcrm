package com.yapikredi.crm.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.*;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.time.Instant;
import java.time.format.DateTimeFormatter;
import java.util.Objects;
import java.util.UUID;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "annual_leaves")
@Entity
public class AnnualLeaveEntity {
    @Id
    private UUID id;
    private UUID employerId;
    private Instant startAnnualDate;
    private Instant endAnnualDate;
    private Integer annualDays;
    @Enumerated(EnumType.STRING)
    private ApprovalStatus status;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        AnnualLeaveEntity that = (AnnualLeaveEntity) o;
        return Objects.equals(id, that.id) && Objects.equals(employerId, that.employerId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, employerId);
    }
}