package com.yapikredi.crm.domain;

import java.util.Objects;

public abstract class BaseEntity<ID> {
    private ID id;

    public ID getId() {
        return id;
    }

    public void setId(ID id) {
        this.id = id;
    }

    @Override
    public boolean equals(Object o) {
        if(this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        BaseEntity<?> be = (BaseEntity<?>) o;
        return id.equals(be.id);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }
}
