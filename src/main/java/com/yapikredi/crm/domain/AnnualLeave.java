package com.yapikredi.crm.domain;


import com.yapikredi.crm.entity.ApprovalStatus;

import java.time.Instant;
import java.util.UUID;

public class AnnualLeave extends BaseEntity<UUID> {
    private UUID employerId;
    private Instant startAnnualDate;
    private Instant endAnnualDate;
    private Integer annualDays;
    private ApprovalStatus status;
    private String message;

    private AnnualLeave(Builder builder) {
        employerId = builder.employerId;
        startAnnualDate = builder.startAnnualDate;
        endAnnualDate = builder.endAnnualDate;
        annualDays = builder.annualDays;
        status = builder.status;
        message = builder.message;
        setId(builder.id);
    }

    public UUID getEmployerId() {
        return employerId;
    }

    public Instant getStartAnnualDate() {
        return startAnnualDate;
    }

    public Instant getEndAnnualDate() {
        return endAnnualDate;
    }

    public ApprovalStatus getStatus() {
        return status;
    }

    public String getMessage() {
        return message;
    }

    public Integer getAnnualDays() {
        return annualDays;
    }

    public static Builder builder() {
        return new Builder();
    }

    public static final class Builder {
        private UUID employerId;
        private Instant startAnnualDate;
        private Instant endAnnualDate;
        private ApprovalStatus status;
        private Integer annualDays;
        private String message;
        private UUID id;

        private Builder() {
        }

        public Builder employerId(UUID val) {
            employerId = val;
            return this;
        }

        public Builder startAnnualDate(Instant val) {
            startAnnualDate = val;
            return this;
        }

        public Builder endAnnualDate(Instant val) {
            endAnnualDate = val;
            return this;
        }

        public Builder annualDays(Integer val) {
            annualDays = val;
            return this;
        }

        public Builder status(ApprovalStatus val) {
            status = val;
            return this;
        }

        public Builder message(String val) {
            message = val;
            return this;
        }

        public Builder id(UUID val) {
            id = val;
            return this;
        }

        public AnnualLeave build() {
            return new AnnualLeave(this);
        }
    }
}