package com.yapikredi.crm.domain;

import java.time.Instant;
import java.util.UUID;

public class Employer {
    private UUID id;
    private String name;
    private String surname;
    private Instant started_at;

    private Employer(Builder builder) {
        id = builder.id;
        name = builder.name;
        surname = builder.surname;
        started_at = builder.started_at;
    }

    public UUID getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getSurname() {
        return surname;
    }

    public Instant getStarted_at() {
        return started_at;
    }

    public static Builder builder() {
        return new Builder();
    }

    public static final class Builder {
        private UUID id;
        private String name;
        private String surname;
        private Instant started_at;

        private Builder() {
        }

        public Builder id(UUID val) {
            id = val;
            return this;
        }

        public Builder name(String val) {
            name = val;
            return this;
        }

        public Builder surname(String val) {
            surname = val;
            return this;
        }

        public Builder started_at(Instant val) {
            started_at = val;
            return this;
        }

        public Employer build() {
            return new Employer(this);
        }
    }
}