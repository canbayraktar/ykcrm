package com.yapikredi.crm.service;

import com.yapikredi.crm.domain.Employer;
import com.yapikredi.crm.dto.create.CreateAnnualCommand;
import com.yapikredi.crm.dto.create.CreateAnnualResponse;
import com.yapikredi.crm.dto.create.CreateApproveAnnualCommand;

import java.time.Instant;
import java.util.UUID;

public interface EmployerAnnualApplicationService {

    Employer getEmployerById(final UUID employerId);

    CreateAnnualResponse createAnnualLeave(final CreateAnnualCommand createAnnualCommand);
    CreateAnnualResponse approvementAnnual(final CreateApproveAnnualCommand createApproveAnnualCommand);
}