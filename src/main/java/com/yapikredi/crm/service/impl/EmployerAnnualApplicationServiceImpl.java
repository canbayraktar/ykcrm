package com.yapikredi.crm.service.impl;

import com.yapikredi.crm.domain.Employer;
import com.yapikredi.crm.dto.create.CreateAnnualCommand;
import com.yapikredi.crm.dto.create.CreateAnnualResponse;
import com.yapikredi.crm.dto.create.CreateApproveAnnualCommand;
import com.yapikredi.crm.handler.EmployeAnnuanLeaveHelper;
import com.yapikredi.crm.service.EmployerAnnualApplicationService;
import org.springframework.stereotype.Service;

import java.util.UUID;

@Service
public class EmployerAnnualApplicationServiceImpl implements EmployerAnnualApplicationService {

    private EmployeAnnuanLeaveHelper employeAnnuanLeaveHelper;

    public EmployerAnnualApplicationServiceImpl(EmployeAnnuanLeaveHelper employeAnnuanLeaveHelper) {
        this.employeAnnuanLeaveHelper = employeAnnuanLeaveHelper;
    }

    @Override
    public Employer getEmployerById(UUID employerId) {
        return employeAnnuanLeaveHelper.getEmployerById(employerId);
    }

    @Override
    public CreateAnnualResponse createAnnualLeave(CreateAnnualCommand createAnnualCommand) {
        return employeAnnuanLeaveHelper.createAnnualLeave(createAnnualCommand);
    }

    @Override
    public CreateAnnualResponse approvementAnnual(CreateApproveAnnualCommand createApproveAnnualCommand) {
        return employeAnnuanLeaveHelper.approvementAnnual(createApproveAnnualCommand);
    }
}