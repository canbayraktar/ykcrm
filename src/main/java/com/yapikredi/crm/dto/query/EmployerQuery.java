package com.yapikredi.crm.dto.query;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;

import javax.validation.constraints.NotNull;
import java.util.UUID;
@Getter
@Builder
@AllArgsConstructor
public class EmployerQuery {
    @NotNull
    private final UUID employerId;
}
