package com.yapikredi.crm.dto.query;

import com.yapikredi.crm.entity.ApprovalStatus;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;

import javax.validation.constraints.NotNull;
import java.util.UUID;

@Getter
@Builder
@AllArgsConstructor
public class AnnualQuery {
    @NotNull
    private final UUID id;
    @NotNull
    private final UUID employerId;
    private ApprovalStatus status;
}