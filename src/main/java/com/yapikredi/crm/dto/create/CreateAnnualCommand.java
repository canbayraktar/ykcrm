package com.yapikredi.crm.dto.create;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.yapikredi.crm.entity.ApprovalStatus;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.NotNull;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.UUID;

@Getter
@Setter
@Builder
@AllArgsConstructor
public class CreateAnnualCommand {
    @NotNull
    private UUID id;
    @NotNull
    private UUID employerId;
    @JsonFormat(pattern = "dd-MM-YYYY")
    private LocalDate startAnnualDate;
    private Instant endAnnualDate;
    private Integer annualDays;
    private ApprovalStatus status;
}