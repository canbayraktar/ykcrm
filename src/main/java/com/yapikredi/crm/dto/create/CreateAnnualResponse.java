package com.yapikredi.crm.dto.create;

import com.yapikredi.crm.entity.ApprovalStatus;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;

import javax.validation.constraints.NotNull;
import java.time.Instant;
import java.util.UUID;

@Getter
@Builder
@AllArgsConstructor
public class CreateAnnualResponse {
    @NotNull
    private final UUID Id;
    @NotNull
    private final UUID employerId;
    private Instant startAnnualDate;
    private Instant endAnnualDate;
    private Integer annualDays;
    private final ApprovalStatus status;
    private final String message;
}