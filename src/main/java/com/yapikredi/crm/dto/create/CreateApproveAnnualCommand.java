package com.yapikredi.crm.dto.create;

import com.yapikredi.crm.entity.ApprovalStatus;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;

import javax.validation.constraints.NotNull;
import java.util.UUID;

@Getter
@Builder
@AllArgsConstructor
public class CreateApproveAnnualCommand {
    @NotNull
    private UUID id;
    @NotNull
    private UUID employerId;
    private ApprovalStatus status;
}
