package com.yapikredi.crm.repository;

import com.yapikredi.crm.entity.AnnualLeaveEntity;
import com.yapikredi.crm.entity.ApprovalStatus;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.UUID;

@Repository
public interface AnnualLeaveRepository extends JpaRepository<AnnualLeaveEntity, UUID> {
    List<AnnualLeaveEntity> getAllByStatus(final ApprovalStatus status);
}