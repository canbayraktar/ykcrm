package com.yapikredi.crm.rest;

import com.yapikredi.crm.dto.create.CreateAnnualCommand;
import com.yapikredi.crm.dto.create.CreateAnnualResponse;
import com.yapikredi.crm.dto.create.CreateApproveAnnualCommand;
import com.yapikredi.crm.service.EmployerAnnualApplicationService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@Slf4j
@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
@RequestMapping(value = "/annual", produces = "application/vnd.api.v1+json")
public class AnnualRestController {

    private EmployerAnnualApplicationService employerAnnualApplicationService;

    public AnnualRestController(EmployerAnnualApplicationService employerAnnualApplicationService) {
        this.employerAnnualApplicationService = employerAnnualApplicationService;
    }

    @PostMapping
    public ResponseEntity<CreateAnnualResponse> annualLeaveRequest(@RequestBody CreateAnnualCommand createAnnualCommand){
        log.info("Creating annual leave for employer: {}", createAnnualCommand.getEmployerId());
        CreateAnnualResponse createAnnualResponse = employerAnnualApplicationService.createAnnualLeave(createAnnualCommand);
        log.info("Annual Leave Creeated as Pending status with id: {}", createAnnualResponse.getId());
        return ResponseEntity.ok(createAnnualResponse);
    }

    @PostMapping("/approvement")
    public ResponseEntity<CreateAnnualResponse> approveAnnualRequest(@RequestBody CreateApproveAnnualCommand createApproveAnnualCommand){
        log.info("Approving approvement annual status changing for employer: {}", createApproveAnnualCommand.getEmployerId());
        CreateAnnualResponse createAnnualResponse = employerAnnualApplicationService.approvementAnnual(createApproveAnnualCommand);
        log.info("Approving approvement annual status changed as status: {}", createAnnualResponse.getStatus().name());
        return ResponseEntity.ok(createAnnualResponse);
    }
}