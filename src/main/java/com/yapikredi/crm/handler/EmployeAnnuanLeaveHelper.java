package com.yapikredi.crm.handler;

import com.yapikredi.crm.domain.Employer;
import com.yapikredi.crm.dto.create.CreateAnnualCommand;
import com.yapikredi.crm.dto.create.CreateAnnualResponse;
import com.yapikredi.crm.dto.create.CreateApproveAnnualCommand;
import com.yapikredi.crm.entity.AnnualLeaveEntity;
import com.yapikredi.crm.entity.EmployerEntity;
import com.yapikredi.crm.mapper.AnnualDataMapper;
import com.yapikredi.crm.repository.AnnualLeaveRepository;
import com.yapikredi.crm.repository.EmployerRepository;
import com.yapikredi.crm.service.EmployerAnnualApplicationService;
import com.yapikredi.crm.util.DateUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.time.Instant;
import java.util.Optional;
import java.util.UUID;

@Slf4j
@Component
public class EmployeAnnuanLeaveHelper {
    private EmployerRepository employerRepository;
    private AnnualDataMapper annualDataMapper;
    private AnnualLeaveRepository annualLeaveRepository;

    public EmployeAnnuanLeaveHelper(EmployerRepository employerRepository,
                                    AnnualDataMapper annualDataMapper,
                                    AnnualLeaveRepository annualLeaveRepository) {
        this.employerRepository = employerRepository;
        this.annualDataMapper = annualDataMapper;
        this.annualLeaveRepository = annualLeaveRepository;
    }

    public CreateAnnualResponse createAnnualLeave(final CreateAnnualCommand createAnnualCommand){
        try {
            Employer employer = getEmployerById(createAnnualCommand.getEmployerId());
            if (employer != null) {
                long totalAnnualCount = this.getAnnualLeaveEntitlementCount(employer.getStarted_at());
                if (totalAnnualCount > 0 && createAnnualCommand.getAnnualDays() <= totalAnnualCount) {
                    Instant endDate = DateUtil.getEndOfAnnualDate(createAnnualCommand.getStartAnnualDate(), createAnnualCommand.getAnnualDays());
                    createAnnualCommand.setEndAnnualDate(endDate);
                    AnnualLeaveEntity annualLeaveEntity = annualDataMapper.createAnnualLeaveCommandToAnnualLeave(createAnnualCommand);
                    annualLeaveRepository.save(annualLeaveEntity);
                    log.info("Annual Request Created!");
                    return annualDataMapper.createAnnuaLeaveToAnnualResponse(annualDataMapper.annualLeaveToAnnualLeaveEntity(annualLeaveEntity));
                }
            }
        }catch (Exception e){
            e.printStackTrace();
        }
        return null;
    }

    public CreateAnnualResponse approvementAnnual(final CreateApproveAnnualCommand createApproveAnnualCommand){
        Optional<AnnualLeaveEntity> annualLeave = annualLeaveRepository.findById(createApproveAnnualCommand.getId());
        if(annualLeave.isPresent()){
            AnnualLeaveEntity annualLeaveEntity = annualLeave.get();
            annualLeaveEntity.setStatus(createApproveAnnualCommand.getStatus());
            annualLeaveRepository.save(annualLeaveEntity);
            return annualDataMapper.annualLeaveToCreateAnnualResponse(annualDataMapper.annualLeaveToAnnualLeaveEntity(annualLeaveEntity));
        }
        return null;
    }

    public Employer getEmployerById(final UUID employerId){
        Optional<EmployerEntity> employerEntity = employerRepository.findById(employerId);
        if(employerEntity.isPresent()){
            return annualDataMapper.employerEntityToEmployer(employerEntity.get());
        }
        return null;
    }

    private long getAnnualLeaveEntitlementCount(final Instant startDate){
        long requesAnnualDay = 0;
        long totalWorkDay = DateUtil.getDayCountOfBetweenTowDate(startDate, Instant.now());

        if(totalWorkDay < 365){
            requesAnnualDay = 5;
        }else if (totalWorkDay >= 365 && totalWorkDay < (5 * 365)){
            requesAnnualDay = 15;
        }else if (totalWorkDay >= (5 * 365) && totalWorkDay < (10 * 365)){
            requesAnnualDay = 18;
        }else if(totalWorkDay >= (10 * 365)){
            requesAnnualDay = 24;
        }
        return requesAnnualDay;
    }
}