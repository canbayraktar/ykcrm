package com.yapikredi.crm.mapper;

import com.yapikredi.crm.domain.AnnualLeave;
import com.yapikredi.crm.domain.Employer;
import com.yapikredi.crm.dto.create.CreateAnnualCommand;
import com.yapikredi.crm.dto.create.CreateAnnualResponse;
import com.yapikredi.crm.entity.AnnualLeaveEntity;
import com.yapikredi.crm.entity.ApprovalStatus;
import com.yapikredi.crm.entity.EmployerEntity;
import com.yapikredi.crm.util.DateUtil;
import org.springframework.stereotype.Component;

import java.util.UUID;

@Component
public class AnnualDataMapper {

    public CreateAnnualResponse createAnnuaLeaveToAnnualResponse(AnnualLeave annualLeave){
        return CreateAnnualResponse.builder()
                .Id(annualLeave.getId())
                .employerId(annualLeave.getEmployerId())
                .startAnnualDate(annualLeave.getStartAnnualDate())
                .endAnnualDate(annualLeave.getEndAnnualDate())
                .annualDays(annualLeave.getAnnualDays())
                .status(annualLeave.getStatus())
                .message(annualLeave.getMessage())
                .build();
    }

    public CreateAnnualResponse annualLeaveToCreateAnnualResponse(AnnualLeave annualLeave){
        return CreateAnnualResponse.builder()
                .Id(annualLeave.getId())
                .employerId(annualLeave.getEmployerId())
                .startAnnualDate(annualLeave.getStartAnnualDate())
                .endAnnualDate(annualLeave.getEndAnnualDate())
                .status(annualLeave.getStatus())
                .annualDays(annualLeave.getAnnualDays())
                .message("")
                .build();
    }

    public AnnualLeaveEntity annualLeaveToAnnualLeaveEntity(AnnualLeave annualLeave) {
        return AnnualLeaveEntity.builder()
                .id(UUID.randomUUID())
                .employerId(annualLeave.getEmployerId())
                .startAnnualDate(annualLeave.getStartAnnualDate())
                .endAnnualDate(annualLeave.getEndAnnualDate())
                .annualDays(annualLeave.getAnnualDays())
                .build();
    }

    public AnnualLeave annualLeaveToAnnualLeaveEntity(AnnualLeaveEntity annualLeaveEntity) {
        return AnnualLeave.builder()
                .id(annualLeaveEntity.getId())
                .employerId(annualLeaveEntity.getEmployerId())
                .startAnnualDate(annualLeaveEntity.getStartAnnualDate())
                .endAnnualDate(annualLeaveEntity.getEndAnnualDate())
                .annualDays(annualLeaveEntity.getAnnualDays())
                .status(annualLeaveEntity.getStatus())
                .build();
    }

    public AnnualLeaveEntity createAnnualLeaveCommandToAnnualLeave(CreateAnnualCommand createAnnualCommand) {
        return AnnualLeaveEntity.builder()
                .id(UUID.randomUUID())
                .employerId(createAnnualCommand.getEmployerId())
                .startAnnualDate(createAnnualCommand.getStartAnnualDate().atStartOfDay(DateUtil.ZONE).toInstant())
                .endAnnualDate(createAnnualCommand.getEndAnnualDate())
                .annualDays(createAnnualCommand.getAnnualDays())
                .status(ApprovalStatus.PENDING)
                .build();
    }

    public Employer employerEntityToEmployer(EmployerEntity employerEntity){
        return Employer.builder()
                .id(employerEntity.getId())
                .name(employerEntity.getName())
                .surname(employerEntity.getSurname())
                .started_at(employerEntity.getStarted_at())
                .build();
    }
}