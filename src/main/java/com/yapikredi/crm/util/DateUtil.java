package com.yapikredi.crm.util;

import java.time.*;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

public class DateUtil {

    private static final String DATE_FORMAT = "dd/MM/yyyy";
    public static final ZoneId ZONE = ZoneId.systemDefault();
    private static final DateTimeFormatter formatter = DateTimeFormatter.ofPattern(DATE_FORMAT)
            .withZone(ZONE);
    public static long getDayCountOfBetweenTowDate(final Instant first, final Instant second){
        return ChronoUnit.DAYS.between(first, second);
    }

    public static List<Instant> getDemoHolidays(){
        List<Instant> holidays = new LinkedList<>();
        LocalDate localDate = LocalDate.now();

        holidays.add(LocalDate.of(localDate.getYear(), localDate.getMonthValue(), 10).atStartOfDay(ZONE).toInstant());
        holidays.add(LocalDate.of(localDate.getYear(), localDate.getMonthValue(), 13).atStartOfDay(ZONE).toInstant());
        holidays.add(LocalDate.of(localDate.getYear(), localDate.getMonthValue(), 18).atStartOfDay(ZONE).toInstant());
        holidays.add(LocalDate.of(localDate.getYear() + 1, 01, 13).atStartOfDay(ZONE).toInstant());
        holidays.add(LocalDate.of(localDate.getYear() + 1, 01, 17).atStartOfDay(ZONE).toInstant());
        holidays.add(LocalDate.of(localDate.getYear() + 1, 01, 18).atStartOfDay(ZONE).toInstant());
        holidays.add(LocalDate.of(localDate.getYear() + 1, 01, 20).atStartOfDay(ZONE).toInstant());
        holidays.add(LocalDate.of(localDate.getYear() + 1, 01, 25).atStartOfDay(ZONE).toInstant());

        return holidays;
    }

    public static Instant getEndOfAnnualDate(final LocalDate startAnnualDate, final long days){
        List<Instant> holidays = DateUtil.getDemoHolidays();
        LocalDate localDate = startAnnualDate;//LocalDateTime.ofInstant(startAnnualDate, ZoneOffset.UTC).toLocalDate(); ;

        long addDays = days;
        int plus = 0;
        while(addDays > 0){
            if(addDays == 0){
                break;
            }
            plus++;
            localDate = localDate.plusDays(1);
            String dayName = localDate.getDayOfWeek().name();
            if(dayName.equals("SATURDAY") || dayName.equals("SUNDAY")){
                System.out.println(plus + ". Weekend : " + dayName);
                continue;
            }else{
                LocalDate finalLocalDate = localDate;
                Optional<Instant> holidayInstant = holidays.stream().filter(holiday -> LocalDateTime.ofInstant(holiday, ZoneOffset.UTC).toLocalDate().format(formatter)
                        .equals(finalLocalDate.format(formatter))).findAny();
                if(holidayInstant.isPresent()) {
                    System.out.println(plus + ". " + formatter.format(holidayInstant.get()) + " Tatil!");
                    continue;
                }
            }
            System.out.println(plus + ". Dayname : " + dayName);

            addDays--;
        }

        Instant instant = localDate.atStartOfDay(ZONE).toInstant();
        System.out.println("end of annual leave : " + formatter.format(instant));
        return instant;
    }
}