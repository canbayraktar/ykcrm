Postgres was used as dbms in this project. firstly create a postgres db then run the init-schema file on the database named postgres.

execute the below insert script


INSERT INTO yapikredi.employers
(id, "name", surname, started_at)
VALUES('6f14f999-f2ee-4b04-b0bd-c90f7f94d4fb', 'Can', 'Bayraktar', '10/10/2021');

for create new annual leaves request  send the postman json data where the annual leave request is created via postman as a post

URL : localhost:8181/annual
Method : POST
Type : JSON

post request Json
{
"employerId":"15e45e7d-3e34-43df-9366-91c66a8cc9ae",
"startAnnualDate":"2022-10-12T16:37:23",
"annualDays":10
}

response json
{
"employerId": "15e45e7d-3e34-43df-9366-91c66a8cc9ae",
"startAnnualDate": "2022-10-11T21:00:00Z",
"endAnnualDate": "2022-10-25T21:00:00Z",
"annualDays": 10,
"status": "PENDING",
"message": null,
"id": "8bb63f56-5bf6-4865-8dc0-a68a4681fa84"
}
#######################
send the following json data via postman to approve or reject the generated annual leave request


URL : localhost:8181/annual/approvement
Method : POST
Type : JSON

post request json
{
"id":"8bb63f56-5bf6-4865-8dc0-a68a4681fa84",
"employerId": "15e45e7d-3e34-43df-9366-91c66a8cc9ae",
"status": "APPROVED"
}

response json
{
"employerId": "15e45e7d-3e34-43df-9366-91c66a8cc9ae",
"startAnnualDate": "2022-10-11T21:00:00Z",
"endAnnualDate": "2022-10-25T21:00:00Z",
"annualDays": 10,
"status": "APPROVED",
"message": "",
"id": "8bb63f56-5bf6-4865-8dc0-a68a4681fa84"
}